CREATE TABLE [dbo].[Productoes](
[Id]     [int]  IDENTITY(1,1) NOT NULL,
[nombre]      VARCHAR (200)   NULL,
[precio]      DECIMAL (10, 2) NULL,
[descripcion] VARCHAR (200)   NULL,

CONSTRAINT [PK_Productoes] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)
)

CREATE TABLE [dbo].[Ventas](
   [Id]     [int]  IDENTITY(1,1) NOT NULL,
    [precio_pedido_sin_iva] DECIMAL (10, 2)  DEFAULT 0,
    [precio_pedido_con_iva] DECIMAL (10, 2)  DEFAULT 0,
    [cliente]               VARCHAR (50)    NULL,

CONSTRAINT [PK_Ventas] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)
)

CREATE TABLE [dbo].[DetalleVenta] (
    [Id]     [int]  IDENTITY(1,1) NOT NULL,
    [id_producto] INT NOT NULL,
    [id_venta]    INT NOT NULL,
    [cantidad]    INT NULL,
	[precio_venta]    DECIMAL (10, 2)  DEFAULT 0,
    FOREIGN KEY ([id_producto]) REFERENCES [dbo].[Productoes] ([Id]) ON DELETE CASCADE,
	FOREIGN KEY ([id_venta]) REFERENCES [dbo].[Ventas] ([Id]) ON DELETE CASCADE,
	CONSTRAINT [PK_DetalleVenta] PRIMARY KEY CLUSTERED 
(
    [Id] ASC
)
);