﻿namespace ApiRest.Entities
{
    public class RespuestaProducto
    {
        public string respuesta { get; set; }

        public string mensaje { get; set; }
        public List<Producto> datos { get; internal set; }

  
    }
}
