﻿using System;
using System.Collections.Generic;

namespace ApiRest.Entities
{
    public partial class Venta
    {
        public int Id { get; set; }
        public decimal? precio_pedido_sin_iva { get; set; }
        public decimal? precio_pedido_con_iva { get; set; }
        public string? Cliente { get; set; }
    }
}
