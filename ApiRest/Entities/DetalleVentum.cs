﻿using System;
using System.Collections.Generic;

namespace ApiRest.Entities
{
    public partial class DetalleVentum
    {
        public int Id { get; set; }
        public int Id_Producto { get; set; }
        public int Id_Venta { get; set; }
        public int? Cantidad { get; set; }
        public decimal precio_venta { get; set; }

        //  public virtual Producto IdProductoNavigation { get; set; } = null!;
        // public virtual Venta IdVentaNavigation { get; set; } = null!;
    }
}
