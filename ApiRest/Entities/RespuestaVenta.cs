﻿namespace ApiRest.Entities
{
    public class RespuestaVenta
    {
        public string respuesta { get; set; }

        public string mensaje { get; set; }
        public List<Venta> datos { get; internal set; }
    }
}
