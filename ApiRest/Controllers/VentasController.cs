﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiRest.Entities;
using ApiRest.Models;

namespace ApiRest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VentasController : ControllerBase
    {
        private readonly pruebaTecnicaContext _context;

        public VentasController(pruebaTecnicaContext context)
        {
            _context = context;
        }

        // GET: api/Ventas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Venta>>> GetVentas()
        {
            return await _context.Ventas.ToListAsync();
        }

        // GET: api/Ventas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Venta>> GetVenta(int id)
        {
            var venta = await _context.Ventas.FindAsync(id);

            if (venta == null)
            {
                return NotFound();
            }

            return venta;
        }

        // PUT: api/Ventas/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVenta(int id, Venta venta)
        {
            if (id != venta.Id)
            {
                return BadRequest();
            }

            _context.Entry(venta).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VentaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Ventas
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Venta>> PostVenta(Venta venta)
        {
            _context.Ventas.Add(venta);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVenta", new { id = venta.Id }, venta);
        }

        // DELETE: api/Ventas/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteVenta(int id)
        {
            var venta = await _context.Ventas.FindAsync(id);
            if (venta == null)
            {
                return NotFound();
            }

            _context.Ventas.Remove(venta);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool VentaExists(int id)
        {
            return _context.Ventas.Any(e => e.Id == id);
        }

        [HttpGet("ListarVentas")]
        public async Task<ActionResult<IEnumerable<Venta>>> ListarVentas()
        {
            var respuestaModel = new RespuestaVenta();
            respuestaModel.respuesta = "OK";
            respuestaModel.mensaje = "Datos Obtenidos";
            var datos = await _context.Ventas.ToListAsync();
            respuestaModel.datos = datos;
            return Ok(respuestaModel);
            
        }

        [Route("CrearVenta")]
        [HttpPost]
        public async Task<ActionResult<Venta>> CrearVenta(Venta venta)
        {
            venta.precio_pedido_con_iva = 0;
            venta.precio_pedido_sin_iva = 0;
            _context.Ventas.Add(venta);
            await _context.SaveChangesAsync();

            var respuestaModel = new RespuestaVenta();
            respuestaModel.respuesta = "OK";
            respuestaModel.mensaje = "Creado exitosamente";

            CreatedAtAction("GetVenta", new { id = venta.Id }, venta);

            return Ok(respuestaModel);
        }

        [Route("AgregarProducto")]
        [HttpPost]
        public async Task<ActionResult<Venta>> AgregarProducto(DetalleVentum DetalleVentum)
        {
            var respuestaModel = new RespuestaVenta();
            var productoModel = await _context.Productoes.FindAsync(DetalleVentum.Id_Producto);
            if (productoModel == null)
            {
                respuestaModel.respuesta = " NO OK";
                respuestaModel.mensaje = "El producto no existe";
                return Ok(respuestaModel);
            }
            var ventaModel = await _context.Ventas.FindAsync(DetalleVentum.Id_Venta);
            if (ventaModel == null)
            {
                respuestaModel.respuesta = " NO OK";
                respuestaModel.mensaje = "El id venta no existe";
                return Ok(respuestaModel);
               
            }
            Decimal precio_producto = (decimal) (productoModel.Precio * DetalleVentum.Cantidad);

            Decimal number1 = (decimal)productoModel.Precio;
            Decimal number2 = 0.13m;

            Decimal iva = number1 * number2;

            DetalleVentum.precio_venta = precio_producto;

            ventaModel.precio_pedido_con_iva = ventaModel.precio_pedido_con_iva +  productoModel.Precio + iva;

            ventaModel.precio_pedido_sin_iva = ventaModel.precio_pedido_sin_iva + precio_producto;

            _context.Entry(ventaModel).State = EntityState.Modified;



            _context.DetalleVenta.Add(DetalleVentum);
            await _context.SaveChangesAsync();
            respuestaModel.respuesta = "OK";
            respuestaModel.mensaje = "Consulta exitosa";
            return Ok(respuestaModel);
        }
        [Route("DetallePedido")]
        [HttpPost]

        public async Task<ActionResult<Venta>> DetallePedido(Venta venta)
        {
            var ventaModel = await _context.Ventas.FindAsync(venta.Id);
            if (ventaModel == null)
            {
                return Ok("El id venta no existe");
            }
            var var = venta.Id;
          
            /*
            var datos = _context.Productoes
                .FromSqlRaw($"SELECT p.id,p.nombre,p.descripcion,p.precio,dv.cantidad,dv.precio_venta  FROM Productoes as p,DetalleVenta as dv  " +
                $"WHERE  dv.id_producto=p.id and dv.id_venta = {var}")
                .ToList();
        
            return Ok(datos);
            /*
            var listado = (from x in esquema.TraSolicitudServicio
                           join s in esquema.TraSolicitud on x.nSolicitudId equals s.nSolicitudId
                           where s.dFecha >= fechaInicio && fechaFin >= s.dFecha && x.nServicioId == item.nServicioId
                           select x).ToList();
            */

            var listado2 = (from p in _context.Productoes
                            join dv in _context.DetalleVenta on p.Id equals dv.Id_Producto
                            where dv.Id_Venta == var 
                            select new { NombreProducto = p.Nombre,
                                         Descripcion = p.Descripcion,
                                         Cantidad = dv.Cantidad,
                                         Precio_venta = dv.precio_venta
                            }).ToList();

         

            return Ok(listado2);
        }
    }
}
