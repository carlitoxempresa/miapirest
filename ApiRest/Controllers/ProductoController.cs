﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ApiRest.Entities;

namespace ApiRest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : ControllerBase
    {
        private readonly pruebaTecnicaContext _context;

        public ProductoController(pruebaTecnicaContext context)
        {
            _context = context;
        }

        // GET: api/Producto
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Producto>>> GetProductoes()
        {
            return await _context.Productoes.ToListAsync();
        }

        // GET: api/Producto/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Producto>> GetProducto(int id)
        {
            var producto = await _context.Productoes.FindAsync(id);

            if (producto == null)
            {
                return NotFound();
            }

            return producto;
        }

        // PUT: api/Producto/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProducto(int id, Producto producto)
        {
            if (id != producto.Id)
            {
                return BadRequest();
            }

            _context.Entry(producto).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Producto
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Producto>> PostProducto(Producto producto)
        {
            _context.Productoes.Add(producto);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProducto", new { id = producto.Id }, producto);
        }

        // DELETE: api/Producto/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProducto(int id)
        {
            var producto = await _context.Productoes.FindAsync(id);
            if (producto == null)
            {
                return NotFound();
            }

            _context.Productoes.Remove(producto);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ProductoExists(int id)
        {
            return _context.Productoes.Any(e => e.Id == id);
        }
        [HttpGet("ListarProductos")]
        public async Task<ActionResult<IEnumerable<Producto>>> ListarProductos()
        {
            var respuestaModel = new RespuestaProducto();
            respuestaModel.respuesta = "OK";
            respuestaModel.mensaje = "Datos Obtenidos";

            var datos=await _context.Productoes.ToListAsync();
            respuestaModel.datos = datos;
            return Ok(respuestaModel);
        }

        [Route("InsertarProducto")]
        [HttpPost]

        public async Task<ActionResult<Producto>> InsertarProducto(Producto producto)
        {
            _context.Productoes.Add(producto);
            await _context.SaveChangesAsync();
            var respuestaModel = new RespuestaProducto();

            respuestaModel.respuesta = "OK";
            respuestaModel.mensaje = "Se añadio el producto";

            CreatedAtAction("InsertarProducto", new { id = producto.Id }, producto);

            return Ok(respuestaModel);
        }

        [Route("EliminarProducto")]
        [HttpPost]


        public async Task<ActionResult<Producto>> EliminarProducto(Producto producto)
        {
            var respuestaModel = new RespuestaProducto();

   

            var productoModel = await _context.Productoes.FindAsync(producto.Id);
            if (productoModel == null)
            {

                respuestaModel.respuesta = "NO OK";
                respuestaModel.mensaje = "El producto no existe";
                return Ok(respuestaModel);
            }

            _context.Productoes.Remove(productoModel);
            await _context.SaveChangesAsync();
            respuestaModel.respuesta = "OK";
            respuestaModel.mensaje = "Producto eliminado";

            return Ok(respuestaModel);
            //return CreatedAtAction("Eliminado", new { producto } );
           // return productoModel();
        }


        [Route("EditarProducto")]
        [HttpPost]

        public async Task<IActionResult> EditarProducto(Producto producto)
        {
            var respuestaModel = new RespuestaProducto();
            _context.Entry(producto).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductoExists(producto.Id))
                {
                    respuestaModel.respuesta = "NO OK";
                    respuestaModel.mensaje = "El producto no existe";
                    return Ok(respuestaModel);
                }
                else
                {
                    throw;
                }
            }
            respuestaModel.respuesta = "OK";
            respuestaModel.mensaje = "Producto editado";
            return Ok(respuestaModel);
        }
    }
}
